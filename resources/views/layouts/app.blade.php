<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/itgeek.css') }}">
    <link rel="stylesheet" href="{{ asset('css/article.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Fredoka+One&family=Roboto:wght@300;400&display=swap" rel="stylesheet">
    <title>@yield('title')</title>
</head>
<body>
    @include('include.header')
    @yield('section')
    @yield('sectionArticle')
</body>
</html>
